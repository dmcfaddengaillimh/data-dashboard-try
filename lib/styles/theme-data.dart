import 'dart:ui';

import 'package:flutter/material.dart';

class Styles {
  static ThemeData themeData(bool isDarkTheme, BuildContext context) {
    return ThemeData(

      primaryColor: isDarkTheme ? Color(0xffC53DEB) : Color(0xff3C505D),
      primaryColorLight: isDarkTheme ? Color(0xffEEC5F9) : Color(0xffC5CBCE),
      primaryColorDark: isDarkTheme ? Color(0xffB027E2) : Color(0xff273741),

      accentColor: isDarkTheme ? Color(0xff0075FF) : Color(0xffBF6C5D),

      backgroundColor: isDarkTheme ? Color(0xff2A2A2A) : Color(0xffF0F0F0),
      scaffoldBackgroundColor: isDarkTheme ? Color(0xff2A2A2A) : Color(0xffF0F0F0),
      canvasColor: isDarkTheme ? Color(0xff2A2A2A) : Color(0xffFAFAFA),
      cardColor: isDarkTheme ? Color(0xff575757) : Color(0xffFAFAFA),
      dialogBackgroundColor: isDarkTheme ? Color(0xff575757) : Color(0xffF0F0F0),

      hintColor: isDarkTheme ? Colors.white : Colors.black,

      inputDecorationTheme: Theme.of(context).inputDecorationTheme.copyWith(
        hintStyle: isDarkTheme ? TextStyle(color: Colors.white) : TextStyle(color: Colors.black),
        focusedBorder: isDarkTheme ? UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)) : UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
      ),

      iconTheme: IconThemeData(
        color: isDarkTheme ? Colors.white : Colors.black
      ),

      chipTheme: Theme.of(context).chipTheme.copyWith(
        backgroundColor: Theme.of(context).primaryColor,
        disabledColor: Colors.grey,
        labelStyle: TextStyle(
          color: Colors.white,
        ),
        selectedColor: isDarkTheme ? Color(0xff0075FF) : Color(0xffBF6C5D),
        padding: const EdgeInsets.all(10)
      ),

      sliderTheme: Theme.of(context).sliderTheme.copyWith(
        showValueIndicator: ShowValueIndicator.always,
      ),

      buttonTheme: Theme.of(context).buttonTheme.copyWith(
        colorScheme: isDarkTheme ? ColorScheme.dark() : ColorScheme.light(),
      ),

      fontFamily: 'Roboto',
      disabledColor: Colors.grey,
      // bottomSheetTheme: BottomSheetThemeData(backgroundColor: Colors.black54),

      textTheme: TextTheme(
        headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold, color: isDarkTheme ? Colors.white : Colors.black),
        subhead: TextStyle(fontSize: 14.0, color: isDarkTheme ? Colors.white : Colors.white),
        title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic, color: isDarkTheme ? Colors.white : Colors.black),
        caption: TextStyle(fontSize: 14.0, color: isDarkTheme ? Colors.white : Colors.black),
        body1: TextStyle(fontSize: 16.0, color: isDarkTheme ? Colors.white : Colors.black),
        body2: TextStyle(fontSize: 14.0, color: isDarkTheme ? Colors.white : Colors.black)
      ).apply(
        bodyColor: isDarkTheme ? Colors.white : Colors.black,
        displayColor: Colors.grey[500],
      )
    );

  }
}
