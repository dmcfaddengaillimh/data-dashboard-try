import 'dart:ui';
import 'package:flutter/src/painting/text_style.dart';

abstract class HeaderStyles {
  static var headlineText = new TextStyle(
    color: Color.fromRGBO(255, 255, 255, 0.8),
    fontSize: 32,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.bold,
  );
}
