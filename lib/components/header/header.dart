import 'package:flutter/material.dart';
import 'package:data_dashboard_try/styles/textStyles.dart' as headerStyles;

class Header extends StatelessWidget {
  final String headerText;

  Header({
    @required this.headerText,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(this.headerText, style: headerStyles.HeaderStyles.headlineText),
    );
  }
}
