import 'package:flutter/material.dart';

import 'package:data_dashboard_try/components/authentication/signup/signup.dart';

import 'package:data_dashboard_try/pages/home/home.dart';

import 'package:firebase_auth/firebase_auth.dart' show FirebaseAuth, FirebaseUser;

class LogInPage extends StatefulWidget {
  @override
  _LogInPageState createState() => new _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {

  String _email;
  String _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  FocusNode emailFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool passwordVisible = false;

  @override
  void initState() {
    if(this.mounted) {
      setState(() {
        passwordVisible = !passwordVisible;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('Log In'),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            _dismissKeyboard(context);
          },
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: TextFormField(
                    validator: (emailInput) {
                      if(emailInput.isEmpty) {
                        return 'Please enter an email.';
                      } else {
                          this._email = emailInput;
                        }
                    },
                    decoration: InputDecoration(
                      labelText: 'Email',
                      hintText: 'example@provider.com',
                      prefixIcon: Icon(Icons.email, color: Theme.of(context).primaryColor),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor
                        )
                      )
                    ),
                    controller: _emailController,
                    onSaved: (emailInput) => this._email = emailInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, emailFocusNode, passwordFocusNode);
                    },
                    focusNode: emailFocusNode,
                    autofocus: true,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: TextFormField(
                    validator: (passwordInput) {
                      if(passwordInput.length < 5) {
                        return 'Please use a longer password please.';
                      } else {
                          this._password = passwordInput;
                        }
                    },
                    decoration: InputDecoration(
                      labelText: 'Password',
                      prefixIcon: Icon(Icons.lock_outline, color: Theme.of(context).primaryColor),
                      suffixIcon: IconButton(
                        icon: new Icon(passwordVisible ? Icons.visibility : Icons.visibility_off, color: Theme.of(context).primaryColor),
                        onPressed: initState,
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor
                        )
                      )
                    ),
                    controller: _passwordController,
                    onSaved: (passwordInput) => this._password = passwordInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, passwordFocusNode, null);
                    },
                    focusNode: passwordFocusNode,
                    obscureText: passwordVisible,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.go
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: RaisedButton(
                    onPressed: logIn,
                    child: Text('Log In'),
                    color: Theme.of(context).accentColor,
                    textColor: Colors.white,
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, left: 0, right: 0, bottom: 40),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Don\'t have an account?'),
                        FlatButton(
                          child: Text('Sign up', style: TextStyle(color: Theme.of(context).accentColor)),
                          onPressed: () {
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignUpPage(), fullscreenDialog: true));
                          },
                        ),
                      ],
                    ),
                  )
                )
              ],
            ),
          )
        )
      )
    );
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  _dismissKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  void logIn() async {
    final formState = _formKey.currentState;

    if(formState.validate()) {
      formState.save();
      try {
        FirebaseUser user = (await FirebaseAuth.instance.signInWithEmailAndPassword(email: _email, password: _password)).user;
        Navigator.push(context, MaterialPageRoute(builder: (context) => Home(user: user)));
      } catch(error) {
        print(error.message);
      }
    }

  }

}
