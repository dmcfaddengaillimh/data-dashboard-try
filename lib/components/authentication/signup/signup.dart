import 'dart:io';
import 'package:flutter/material.dart';

import 'package:data_dashboard_try/components/authentication/login/login.dart';
import 'package:data_dashboard_try/components/authentication/signup/information.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart' show FirebaseAuth;

import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => new _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  File _image;
  String _email;
  String _password;
  String _repeatPassword;
  String _downloadURL;

  int minimumEmailLength = 6;
  int minimumPasswordLength = 5;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _pass = TextEditingController();
  final TextEditingController _confirmPass = TextEditingController();

  FocusNode emailFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();
  FocusNode confirmPasswordFocusNode = new FocusNode();

  bool passwordVisible = false;

  @override
  void initState() {
    setState(() {
      passwordVisible = !passwordVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('Sign Up'),
      ),
      body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              _dismissKeyboard(context);
            },
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 10, top: 40, right: 35, bottom: 0),
                        child: CircleAvatar(
                          radius: 50,
                          backgroundColor: Color(0xffe6e3e3),
                          child: ClipOval(
                            child: new SizedBox(
                              width: 200.0,
                              height: 200.0,
                              child: (_image != null) ? Image.file(
                                _image,
                                fit: BoxFit.fill,
                              ) : Image.network(
                                "https://img.icons8.com/pastel-glyph/2x/person-male.png",
                                fit: BoxFit.fill
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 0, top: 40, right: 0, bottom: 0),
                        child: RaisedButton.icon(
                          color: Theme.of(context).primaryColorDark,
                          textColor: Colors.white,
                          onPressed: getPhoto,
                          icon: Icon(Icons.camera_alt),
                          label: Text('Upload Photo')
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 40, right: 40, bottom: 30),
                    child: TextFormField(
                      validator: (emailInput) {
                        if(emailInput.isEmpty || emailInput.length < minimumEmailLength){
                          return 'Please enter an email.';
                        }
                      },
                      decoration: InputDecoration(
                        labelText: 'Email',
                        hintText: 'example@provider.com',
                        prefixIcon: Icon(Icons.email, color: Theme.of(context).primaryColor),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor
                          )
                        )
                      ),
                      onSaved: (emailInput) => _email = emailInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, emailFocusNode, passwordFocusNode);
                      },
                      focusNode: emailFocusNode,
                      autofocus: true,
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                    child: TextFormField(
                      controller: _pass,
                      validator: (passwordInput) {
                        if(passwordInput.length < minimumPasswordLength) {
                          return 'Please enter a longer password.';
                        }
                      },
                      decoration: InputDecoration(
                        labelText: 'Password',
                        prefixIcon: Icon(Icons.lock_outline, color: Theme.of(context).primaryColor),
                        suffixIcon: IconButton(
                          icon: new Icon(passwordVisible ? Icons.visibility : Icons.visibility_off, color: Theme.of(context).primaryColor),
                          onPressed: initState,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor
                          )
                        )
                      ),
                      onSaved: (passwordInput) => _password = passwordInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, passwordFocusNode, confirmPasswordFocusNode);
                      },
                      focusNode: passwordFocusNode,
                      obscureText: passwordVisible,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                    child: TextFormField(
                      controller: _confirmPass,
                      validator: (repeatPasswordInput) {
                        if(repeatPasswordInput.length < minimumPasswordLength) {
                          return 'Please enter a longer password.';
                        }
                        if(repeatPasswordInput != _pass.text) {
                          return 'Passwords do not match.';
                        }
                      },
                      decoration: InputDecoration(
                        labelText: 'Repeat Password',
                        prefixIcon: Icon(Icons.lock_outline, color: Theme.of(context).primaryColor),
                        suffixIcon: IconButton(
                          icon: new Icon(passwordVisible ? Icons.visibility : Icons.visibility_off, color: Theme.of(context).primaryColor),
                          onPressed: initState,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor
                          )
                        )
                      ),
                      onSaved: (repeatPasswordInput) => _repeatPassword = repeatPasswordInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, confirmPasswordFocusNode, null);
                      },
                      focusNode: confirmPasswordFocusNode,
                      obscureText: passwordVisible,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.go
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: RaisedButton(
                      onPressed: signUp,
                      child: Text('Sign up'),
                      color: Theme.of(context).accentColor,
                      textColor: Colors.white,
                    ),
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10, left: 0, right: 0, bottom: 40),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Already have an account?'),
                          FlatButton(
                            child: Text('Login', style: TextStyle(color: Theme.of(context).accentColor)),
                            onPressed: () {
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LogInPage()));
                            },
                          ),
                        ],
                      ),
                    )
                  )
                ],
              ),
            )
          )
        )
      );
    }

    Future getPhoto() async {
      File img = await ImagePicker.pickImage(source: ImageSource.gallery);

      setState(() {
        _image = img;
      });
    }

    void uploadToStorage(user) async {
      String fileName = Path.basename(this._image.path);

      StorageReference ref = FirebaseStorage.instance.ref().child('user_profile_images/${user.user.uid}/' + fileName);

      StorageUploadTask uploadTask = ref.putFile(this._image);

      var downloadURL = await (await uploadTask.onComplete).ref.getDownloadURL();
      String fileURL = downloadURL.toString();
      this._downloadURL = fileURL;

      StorageTaskSnapshot checkComplete = await uploadTask.onComplete;

      Firestore.instance.collection('users').document(user.user.uid).updateData({"profilePhotoURL": _downloadURL});
    }

    _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
      currentFocus.unfocus();
      FocusScope.of(context).requestFocus(nextFocus);
    }

    _dismissKeyboard(BuildContext context) {
      FocusScope.of(context).requestFocus(new FocusNode());
    }

    void signUp() async {
      final formState = _formKey.currentState;

      if(formState.validate()) {
        formState.save();
        try {
          await FirebaseAuth.instance.createUserWithEmailAndPassword(email: _email, password: _password)
            .then((user) => {
              uploadToStorage(user),
              Firestore.instance.collection('users').document(user.user.uid).setData({"email": _email, "password": _password, "profilePhotoURL": _downloadURL}),
            });
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ExtraSignUpInfo()));
        } catch(error) {
          print(error.message);
        }
      }
    }

  }
