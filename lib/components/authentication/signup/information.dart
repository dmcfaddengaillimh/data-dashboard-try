import 'package:flutter/material.dart';

import 'package:data_dashboard_try/pages/home/home.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ExtraSignUpInfo extends StatefulWidget {
  @override
  _ExtraSignUpInfoState createState() => _ExtraSignUpInfoState();
}

class _ExtraSignUpInfoState extends State<ExtraSignUpInfo> {
  String _firstName;
  String _lastName;
  String _company;
  String _jobTitle;
  String _teamName;
  String _industry;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  FocusNode firstNameFocusNode = new FocusNode();
  FocusNode lastNameFocusNode = new FocusNode();
  FocusNode companyFocusNode = new FocusNode();
  FocusNode jobTitleFocusNode = new FocusNode();
  FocusNode teamNameFocusNode = new FocusNode();
  FocusNode industryFocusNode = new FocusNode();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('Extra Info'),
      ),
      body: SingleChildScrollView(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            _dismissKeyboard(context);
          },
            child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 40, right: 40, bottom: 30),
                  child: TextFormField(
                    validator: (firstNameInput) {
                      if(firstNameInput.isEmpty) {
                        return 'Please enter your first name.';
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'First Name',
                      prefixIcon: Icon(Icons.person, color: Theme.of(context).primaryColor),
                      focusedBorder: OutlineInputBorder()
                    ),
                    onSaved: (firstNameInput) => _firstName = firstNameInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, firstNameFocusNode, lastNameFocusNode);
                    },
                    focusNode: firstNameFocusNode,
                    autofocus: true,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: TextFormField(
                    validator: (lastNameInput) {
                      if(lastNameInput.isEmpty) {
                        return 'Please enter your last name.';
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Last Name',
                      prefixIcon: Icon(Icons.person, color: Theme.of(context).primaryColor),
                      focusedBorder: OutlineInputBorder()
                    ),
                    onSaved: (lastNameInput) => _lastName = lastNameInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, lastNameFocusNode, companyFocusNode);
                    },
                    focusNode: lastNameFocusNode,
                    autofocus: true,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: TextFormField(
                    validator: (companyInput) {
                      if(companyInput.isEmpty) {
                        return 'Please enter your company.';
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Company',
                      hintText: 'XYZ Inc.',
                      prefixIcon: Icon(Icons.business, color: Theme.of(context).primaryColor),
                      focusedBorder: OutlineInputBorder()
                    ),
                    onSaved: (companyInput) => _company = companyInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, companyFocusNode, jobTitleFocusNode);
                    },
                    focusNode: companyFocusNode,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: TextFormField(
                    validator: (jobTitleInput) {
                      if(jobTitleInput.isEmpty) {
                        return 'Please enter your job title.';
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Job Title',
                      prefixIcon: Icon(Icons.branding_watermark, color: Theme.of(context).primaryColor),
                      focusedBorder: OutlineInputBorder()
                    ),
                    onSaved: (jobTitleInput) => _jobTitle = jobTitleInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, jobTitleFocusNode, teamNameFocusNode);
                    },
                    focusNode: jobTitleFocusNode,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: TextFormField(
                    validator: (teamNameInput) {
                      if(teamNameInput.isEmpty) {
                        return 'Please enter your teams name.';
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Team Name',
                      prefixIcon: Icon(Icons.people, color: Theme.of(context).primaryColor),
                      focusedBorder: OutlineInputBorder()
                    ),
                    onSaved: (teamNameInput) => _teamName = teamNameInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, teamNameFocusNode, industryFocusNode);
                    },
                    focusNode: teamNameFocusNode,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: TextFormField(
                    validator: (industryInput) {
                      if(industryInput.isEmpty) {
                        return 'Please enter your industry.';
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Industry',
                      prefixIcon: Icon(Icons.business_center, color: Theme.of(context).primaryColor),
                      focusedBorder: OutlineInputBorder()
                    ),
                    onSaved: (industryInput) => _industry = industryInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, industryFocusNode, null);
                    },
                    focusNode: industryFocusNode,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.go
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: RaisedButton(
                    onPressed: finishSignUp,
                    child: Text('Sign up'),
                    color: Theme.of(context).accentColor,
                    textColor: Colors.white,
                  ),
                )
              ],
            )
          )
        )
      )
    );
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  _dismissKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  void finishSignUp() async {
    final formState = _formKey.currentState;

    if(formState.validate()) {
      formState.save();
      try {
        FirebaseUser user = await FirebaseAuth.instance.currentUser();
        await Firestore.instance.collection('users').document(user.uid).updateData({
          "name": {
            "firstName": this._firstName,
            "lastName": this._lastName
          },
          "company": this._company,
          "jobTitle": this._jobTitle,
          "teamName": this._teamName,
          "industry": this._industry
        });
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Home(user: user)));
      } catch(error) {
        print(error.message);
      }
    }

  }
}
