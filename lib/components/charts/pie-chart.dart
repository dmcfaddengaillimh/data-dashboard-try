import 'package:data_dashboard_try/models/activity.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DonutActivityChart extends StatelessWidget {
  final List<Activity> myData;

  DonutActivityChart({this.myData});

  @override
  Widget build(BuildContext context) {
    List<charts.Series<Activity, int> > seriesData = [
      charts.Series(
        id: 'Activity Name',
        data: myData,
        domainFn: (Activity activity, _) => activity.timeSpent,
        measureFn: (Activity activity, _) => activity.distractedTime,
        colorFn: (Activity activity, _) => charts.ColorUtil.fromDartColor(Color(int.parse(activity.colour)))
      )
    ];

    return Container(
      height: 250,
        child: Column(
          children: <Widget>[
            Expanded(
              child: charts.PieChart(
                seriesData,
                animate: true,
                animationDuration: Duration(seconds: 1),
                defaultRenderer: new charts.ArcRendererConfig(
                  arcWidth: 35,
                  arcRendererDecorators: [
                    new charts.ArcLabelDecorator(
                      labelPosition: charts.ArcLabelPosition.inside)
                    ]
                  ),
                ),
              )
          ],
        )
    );
  }
}
