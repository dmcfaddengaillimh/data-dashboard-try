class Activity {
  final int timeSpent;
  final int distractedTime;
  final bool isProductive;
  final String colour;

  Activity(this.timeSpent, this.distractedTime, this.isProductive, this.colour);

  Activity.fromMap(Map<String, dynamic> map)
  :assert(map['timeSpent'] != null),
   assert(map['distractedTime'] != null),
   assert(map['isProductive'] != null),
   assert(map['colour'] != null),
  timeSpent = map['timeSpent'],
   distractedTime = map['distractedTime'],
   isProductive = map['isProductive'],
   colour = map['colour'];
}
