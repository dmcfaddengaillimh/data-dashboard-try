import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_dashboard_try/pages/home/home.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  String _updatedFirstName;
  String _updatedLastName;
  String _updatedCompany;
  String _updatedJobTitle;
  String _updatedTeamName;
  String _updatedIndustry;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  FocusNode firstNameFocusNode = new FocusNode();
  FocusNode lastNameFocusNode = new FocusNode();
  FocusNode jobTitleFocusNode = new FocusNode();
  FocusNode companyFocusNode = new FocusNode();
  FocusNode teamNameFocusNode = new FocusNode();
  FocusNode industryFocusNode = new FocusNode();

  DocumentSnapshot userSnapshot;

  final TextEditingController _firstNameController = new TextEditingController();
  final TextEditingController _lastNameController = new TextEditingController();
  final TextEditingController _jobTitleController = new TextEditingController();
  final TextEditingController _companyController = new TextEditingController();
  final TextEditingController _teamNameController = new TextEditingController();
  final TextEditingController _industryController = new TextEditingController();

  @override
  void initState() {
    getInitialDetails();
    return super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('Profile')
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            _dismissKeyboard(context);
          },
          child: Form(
            key: _formKey,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                    child: TextFormField(
                      validator: (updatedFirstNameInput) {
                        if(updatedFirstNameInput.isEmpty) {
                          return 'Please enter an updated first name.';
                        } else {
                          this._updatedFirstName = updatedFirstNameInput;
                        }
                      },
                      decoration: InputDecoration(
                        labelText: 'First Name',
                        prefixIcon: Icon(Icons.person, color: Theme.of(context).primaryColor),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor
                          )
                        )
                      ),
                      controller: _firstNameController,
                      onChanged: (updatedFirstNameInput) => this._updatedFirstName = updatedFirstNameInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, firstNameFocusNode, lastNameFocusNode);
                      },
                      focusNode: firstNameFocusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.go
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                    child: TextFormField(
                      validator: (updatedLastNameInput) {
                        if(updatedLastNameInput.isEmpty) {
                          return 'Please enter an updated last name.';
                        } else {
                          this._updatedLastName = updatedLastNameInput;
                        }
                      },
                      decoration: InputDecoration(
                        labelText: 'Last Name',
                        prefixIcon: Icon(Icons.person, color: Theme.of(context).primaryColor),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor
                          )
                        )
                      ),
                      controller: _lastNameController,
                      onChanged: (updatedlastNameInput) => this._updatedLastName = updatedlastNameInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, lastNameFocusNode, jobTitleFocusNode);
                      },
                      focusNode: lastNameFocusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.go
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                    child: TextFormField(
                      validator: (updatedJobTitleInput) {
                        if(updatedJobTitleInput.isEmpty) {
                          return 'Please enter an updated job title.';
                        } else {
                          this._updatedJobTitle = updatedJobTitleInput;
                        }
                      },
                      decoration: InputDecoration(
                        labelText: 'Job Title',
                        prefixIcon: Icon(Icons.branding_watermark, color: Theme.of(context).primaryColor),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor
                          )
                        )
                      ),
                      controller: _jobTitleController,
                      onChanged: (updatedJobTitleInput) => this._updatedJobTitle = updatedJobTitleInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, jobTitleFocusNode, companyFocusNode);
                      },
                      focusNode: jobTitleFocusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.go
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                    child: TextFormField(
                      validator: (updatedCompanyInput) {
                        if(updatedCompanyInput.isEmpty) {
                          return 'Please enter an updated company.';
                        } else {
                          this._updatedCompany = updatedCompanyInput;
                        }
                      },
                      decoration: InputDecoration(
                        labelText: 'Company',
                        prefixIcon: Icon(Icons.business, color: Theme.of(context).primaryColor),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor
                          )
                        )
                      ),
                      controller: _companyController,
                      onChanged: (updatedCompanyInput) => this._updatedCompany = updatedCompanyInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, companyFocusNode, teamNameFocusNode);
                      },
                      focusNode: companyFocusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.go
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                    child: TextFormField(
                      validator: (updatedTeamNameInput) {
                        if(updatedTeamNameInput.isEmpty) {
                          return 'Please enter an updated team name.';
                        } else {
                          this._updatedTeamName = updatedTeamNameInput;
                        }
                      },
                      decoration: InputDecoration(
                        labelText: 'Team Name',
                        prefixIcon: Icon(Icons.people, color: Theme.of(context).primaryColor),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor
                          )
                        )
                      ),
                      controller: _teamNameController,
                      onChanged: (updatedTeamNameInput) => this._updatedTeamName = updatedTeamNameInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, teamNameFocusNode, industryFocusNode);
                      },
                      focusNode: teamNameFocusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.go
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                    child: TextFormField(
                      validator: (updatedIndustryInput) {
                        if(updatedIndustryInput.isEmpty) {
                          return 'Please enter an updated industry.';
                        } else {
                          this._updatedIndustry = updatedIndustryInput;
                        }
                      },
                      decoration: InputDecoration(
                        labelText: 'Industry',
                        prefixIcon: Icon(Icons.business_center, color: Theme.of(context).primaryColor),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor
                          )
                        )
                      ),
                      controller: _industryController,
                      onChanged: (updatedIndustryInput) => this._updatedIndustry = updatedIndustryInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, teamNameFocusNode, null);
                      },
                      focusNode: industryFocusNode,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.go
                    ),
                  ),
                  RaisedButton(
                    onPressed: () => updateUserProfileDetails(context),
                    child: Text('Save', style: TextStyle(fontSize: 14, color: Colors.white)),
                    color: Theme.of(context).accentColor,
                  )
                ],
              ),
            ),
          ),
        )
      )
    );
  }

  void getUsersDetails() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String userId = user.uid.toString();
    DocumentReference userDoc = Firestore.instance.collection('users').document(userId);
    userDoc.get().then((doc) {
      if (doc.exists) {
        setState(() {
          userSnapshot = doc.data as DocumentSnapshot;
        });
    } else {
        print("No such document!");
    }
    });
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  _dismissKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  updateUserProfileDetails(BuildContext context) async {
    final formState = _formKey.currentState;
    bool successfullSubmissionOfChanges;

    if(formState.validate()) {
      formState.save();
      try {
        FirebaseUser user = await FirebaseAuth.instance.currentUser();
        String userId = user.uid.toString();

        Firestore.instance.collection('users').document(userId).updateData({
          "company": this._updatedCompany,
          "industry": this._updatedIndustry,
          "jobTitle": this._updatedJobTitle,
          "name": {
            "firstName": this._updatedFirstName,
            "lastName": this._updatedLastName
          },
          "teamName": this._updatedTeamName
        });
        successfullSubmissionOfChanges = true;
        showSnackBar(context, successfullSubmissionOfChanges);
        await Future.delayed(const Duration(seconds: 3));
        Navigator.push(context, new MaterialPageRoute(builder: (context) => Home(user: user)));
      } catch(error) {
        successfullSubmissionOfChanges = false;
        showSnackBar(context, successfullSubmissionOfChanges);
        print(error.message);
      }
    }

  }

  showSnackBar(BuildContext context, bool success) {
    Flushbar(
      title: success ? 'Success' : 'Failure',
      message: success ? 'Your profile details have been updated.' : 'There was an error updating your details.',
      icon: success ? Icon(
        Icons.check_circle_outline,
        size: 28,
        color: Colors.green,
      ) : Icon(
        Icons.remove_circle_outline,
        size: 28,
        color: Colors.red
      ),
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      shouldIconPulse: false,
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      duration: Duration(seconds: 3),
      flushbarStyle: FlushbarStyle.FLOATING
    ) .. show(context);
  }

  getInitialDetails() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String userId = user.uid.toString();

    await Firestore.instance.collection('users').document(userId).get()
      .then((document) {
         if(document.exists) {
           setState(() {
             _firstNameController.text = document.data['name']['firstName'].toString();
             _lastNameController.text = document.data['name']['lastName'].toString();
             _jobTitleController.text = document.data['jobTitle'].toString();
             _companyController.text = document.data['company'].toString();
             _teamNameController.text = document.data['teamName'].toString();
             _industryController.text = document.data['industry'].toString();
           });
         } else {
           return null;
         }
      });
  }
}
