import 'package:charts_flutter/flutter.dart' as charts;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_dashboard_try/models/activity.dart';
import 'package:data_dashboard_try/pages/activity-detail.dart';
import 'package:data_dashboard_try/pages/time-data/time-data.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  List<Activity> myData;
  String _usersID;

  bool productiveValue = true;
  bool unproductiveValue = false;
  bool _productiveSelected = true;
  bool _unproductiveSelected = true;

  RangeValues _values = RangeValues(1, 10);
  int productiveMinutesMin;
  int productiveMinutesMax;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
        actions: <Widget>[
          FlatButton(
            onPressed: () => _showModalBottomSheet(context),
            child: Icon(Icons.settings),
            textColor: Colors.white
          )
        ],
      ),
      body: Flex(
        direction: Axis.vertical,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _buildBody(context),
          Divider(),
          StreamBuilder(
            stream: getUsersActivityStream(context),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return CircularProgressIndicator();
              } else {
                return new Expanded(
                  child: SizedBox(
                    height: 600,
                    child: new ListView.builder(
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (BuildContext context, int index) =>
                        buildActivityCards(context, snapshot.data.documents[index])
                    )
                  )
                );
              }
            }
          )
        ]
      )
    );
  }

  _showModalBottomSheet(BuildContext context) async {
    return showModalBottomSheet(
      isDismissible: true,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Card(
          elevation: 3.0,
          color: Theme.of(context).canvasColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15))
          ),
          child: Container(
            padding: const EdgeInsets.only(left: 16, top: 5, right: 16),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Center(
                    child: Icon(Icons.drag_handle, color: Theme.of(context).textTheme.body1.color)
                  ),
                  SizedBox(height: 5),
                  Text('Productivity'),
                  FilterChoice(
                    productiveValueChanged: (value) {
                      if(this.mounted) {
                        setState(() {
                          _productiveSelected = value;
                          productiveValue = _productiveSelected;
                        });
                      }
                    },
                    unproductiveValueChanged: (value) {
                      if(this.mounted) {
                        setState(() {
                          _unproductiveSelected = value;
                          unproductiveValue = !_unproductiveSelected;
                        });
                      }
                    }
                  ),
                  Text('Active Time'),
                  MinutesSlider(
                    minValue: 1,
                    maxValue: 100,
                    minValueChanged: (minValue) {
                      if(this.mounted) {
                        setState(() {
                          productiveMinutesMin = minValue.toInt();
                        });
                      }
                    },
                    maxValueChanged: (maxValue) {
                      if(this.mounted) {
                        setState(() {
                          productiveMinutesMax = maxValue.toInt();
                        });
                      }
                    }
                  )
                ]
              )
            )
          )
        );
      }
    );
  }

  String truncateLength(String value) {
    String result = value.substring(0, value.indexOf('.'));

    return result;
  }

  Widget _buildBody(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: getUsersCharts(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return LinearProgressIndicator();
        } else {
          List<Activity> activities = snapshot.data.documents
              .map((documentSnapshot) => Activity.fromMap(documentSnapshot.data))
              .toList();
          return _buildChart(context, activities);
        }
      }
    );
  }

  Widget _buildChart(BuildContext context, List<Activity> listOfActivities) {
    myData = listOfActivities;

    List<charts.Series<Activity, int> > seriesData = [
      charts.Series(
        id: 'Activity Name',
        data: listOfActivities,
        domainFn: (Activity activity, _) => activity.timeSpent,
        measureFn: (Activity activity, _) => activity.timeSpent / 1440,
        colorFn: (Activity activity, _) => charts.ColorUtil.fromDartColor(Color(int.parse(activity.colour)))
      )
    ];

    return Container(
      constraints: BoxConstraints(
        minHeight: 250,
        minWidth: 250,
      ),
      height: 300,
      width: 250,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: charts.PieChart(
                seriesData,
                animate: true,
                animationDuration: Duration(seconds: 1),
                defaultRenderer: new charts.ArcRendererConfig(
                  arcWidth: 35,
                  arcRendererDecorators: [
                    new charts.ArcLabelDecorator(
                      labelPosition: charts.ArcLabelPosition.inside
                    )
                  ]
                )
              )
            )
          ]
        )
      )
    );
  }

  Stream<QuerySnapshot> getUsersActivityStream(BuildContext context) async* {
    final uid = await getUsersIDCode();

    if(productiveValue == true && unproductiveValue == false) {
      yield* Firestore.instance.collection('userActivities').document(uid).collection('loggedActivities')
        .where('timeSpent', isGreaterThanOrEqualTo: productiveMinutesMin)
        .where('timeSpent', isLessThanOrEqualTo: productiveMinutesMax)
        .orderBy('timeSpent')
        .orderBy('activityName')
        .snapshots();
    } else {
      yield* Firestore.instance.collection('userActivities').document(uid).collection('loggedActivities')
        .where('isProductive', isEqualTo: _productiveSelected == true ? productiveValue : null)
        .where('isProductive', isEqualTo: _unproductiveSelected == true ? false : null)
        .where('timeSpent', isGreaterThanOrEqualTo: productiveMinutesMin)
        .where('timeSpent', isLessThanOrEqualTo: productiveMinutesMax)
        .orderBy('timeSpent')
        .orderBy('activityName')
        .snapshots();
    }

  }

  Stream<QuerySnapshot> getUsersCharts() async* {
    final uid = await getUsersIDCode();

    if(productiveValue == true && unproductiveValue == false) {
      yield* Firestore.instance.collection('userActivities').document(uid).collection('loggedActivities')
        .where('timeSpent', isGreaterThanOrEqualTo: productiveMinutesMin)
        .where('timeSpent', isLessThanOrEqualTo: productiveMinutesMax)
        .orderBy('timeSpent')
        .orderBy('activityName')
        .snapshots();
    } else {
      yield* Firestore.instance.collection('userActivities').document(uid).collection('loggedActivities')
        .where('isProductive', isEqualTo: _productiveSelected == true ? productiveValue : null)
        .where('isProductive', isEqualTo: _unproductiveSelected == true ? false : null)
        .where('timeSpent', isGreaterThanOrEqualTo: productiveMinutesMin)
        .where('timeSpent', isLessThanOrEqualTo: productiveMinutesMax)
        .orderBy('timeSpent')
        .orderBy('activityName')
        .snapshots();
    }

  }

  Widget buildActivityCards(BuildContext context, DocumentSnapshot activity) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(17.5),
      child: Card(
        shape: Border(left: BorderSide(color: Color(int.parse(activity['colour'])), width: 6)),
        child: ListTile(
          leading: Icon(IconData(activity['activityIcon']['codePoint'], fontFamily: activity['activityIcon']['fontFamily'], matchTextDirection: activity['activityIcon']['matchTextDirection']), color: Theme.of(context).textTheme.body1.color),
          title: Text(activity['activityName'], style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18)),
          subtitle: checkProductive(activity['isProductive']),
          trailing: Column(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.more_vert,
                color: Theme.of(context).textTheme.body1.color),
                onPressed: () => showActivityMenu(activity)
              )
            ]
          )
        )
      )
    );
  }

  Future<Widget> showActivityMenu(DocumentSnapshot documentSnapshot) async {
    return showModalBottomSheet(
      backgroundColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15))
      ),
      context: context,
      builder: (BuildContext context) {
        return Card(
          child: Container(
            padding: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
            child: Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _createListTile(context, 'Edit', Icons.edit, () => _edit(documentSnapshot)),
                  _createListTile(context, 'Delete', Icons.delete, () => _delete(documentSnapshot)),
                  _createListTile(context, 'Detail View', Icons.info_outline, () => _detailView(documentSnapshot))
                ]
              )
            )
          )
        );
      }
    );
  }

  ListTile _createListTile(BuildContext context, String name, IconData icon, Function action) {
    return ListTile(
      leading: Icon(icon, color: Theme.of(context).textTheme.body1.color),
      title: Text(name),
      onTap: () {
        Navigator.pop(context);
        action();
      }
    );
  }

  _edit(DocumentSnapshot snapshot) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => TimeDataPage(editingEntry: true, activitySnapshot: snapshot, snapshotID: snapshot.documentID)));
  }

  _delete(DocumentSnapshot snapshot) async {
    await Firestore.instance.collection('userActivities').document(_usersID).collection('loggedActivities').document(snapshot.documentID).delete();
    setState(() { });
  }

  _detailView(DocumentSnapshot snapshot) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => ActivityDetailView(activitySnapshot: snapshot)));
  }

  checkProductive(bool activityProductiveChoice) {
    if (activityProductiveChoice) {
      return new Text('Productive', style: TextStyle(color: Colors.green, fontSize: 14));
    } else {
      return new Text('Unproductive', style: TextStyle(color: Colors.red, fontSize: 14));
    }
  }

  getUsersIDCode() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String userID = user.uid.toString();
    _usersID = userID;

    return userID;
  }
}

class FilterChoice extends StatefulWidget {
  final String label;
  final ValueChanged productiveValueChanged;
  final ValueChanged unproductiveValueChanged;

  const FilterChoice({
    Key key,
    this.label,
    @required this.productiveValueChanged,
    @required this.unproductiveValueChanged,
  }) : super(key: key);

  @override
  _FilterChoiceState createState() => _FilterChoiceState();
}

class _FilterChoiceState extends State<FilterChoice> {

  bool productiveValue;
  bool unproductiveValue;
  bool _productiveSelected;
  bool _unproductiveSelected;

  @override
  void initState() {
    productiveValue = true;
    unproductiveValue = false;
    _productiveSelected = true;
    _unproductiveSelected = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 10, right: 0, bottom: 20),
      child: Column(
        children: <Widget>[
          Divider(color: Theme.of(context).textTheme.body1.color),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              FilterChip(
                label: Text('Productive'),
                selected: _productiveSelected,
                onSelected: (selected) {
                  _productiveSelected = !_productiveSelected;
                  if(this.mounted) {
                    setState(() {
                      _productiveSelected = selected;
                      productiveValue = _productiveSelected;
                      widget.productiveValueChanged(_productiveSelected);
                    });
                  }
                },
              ),
              FilterChip(
                label: Text('Unproductive'),
                selected: _unproductiveSelected,
                showCheckmark: true,
                onSelected: (selected) {
                  _unproductiveSelected = !_unproductiveSelected;
                  if(this.mounted) {
                    setState(() {
                      _unproductiveSelected = selected;
                      unproductiveValue = !_unproductiveSelected;
                      widget.unproductiveValueChanged(_unproductiveSelected);
                    });
                  }
                }
              )
            ]
          )
        ]
      )
    );
  }
}

class MinutesSlider extends StatefulWidget {
  final ValueChanged minValueChanged;
  final ValueChanged maxValueChanged;
  final int minValue;
  final int maxValue;

  const MinutesSlider({
    Key key,
    @required this.minValue,
    @required this.maxValue,
    @required this.minValueChanged,
    @required this.maxValueChanged,
  }) : super(key: key);

  _MinutesSliderState createState() => _MinutesSliderState();
}

class _MinutesSliderState extends State<MinutesSlider> {
  static int minValue;
  static int maxValue;
  RangeValues _values = RangeValues(1, 100);

  @override
  void initState() {
    minValue = 1;
    maxValue = 100;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 10, right: 0, bottom: 20),
      child: Column(
        children: <Widget>[
          Divider(color: Theme.of(context).textTheme.body1.color),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text(truncateLength('${_values.start}')),
              RangeSlider(
                values: _values,
                min: 1,
                max: 100,
                divisions: 10,
                onChanged: (RangeValues values) {
                  if(this.mounted) {
                    setState(() {
                      _values = values;
                      widget.minValueChanged(values.start);
                      widget.maxValueChanged(values.end);
                    });
                  }
                }
              ),
              Text(truncateLength('${_values.end}')),
            ]
          )
        ]
      )
    );
  }

  String truncateLength(String value) {
    String result = value.substring(0, value.indexOf('.'));

    return result;
  }
}
