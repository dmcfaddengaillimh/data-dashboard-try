import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_dashboard_try/pages/welcome/welcome.dart';
import 'package:data_dashboard_try/styles/dark-theme-provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  @override
  Widget build(BuildContext context) {
    final themeChange = Provider.of<DarkThemeProvider>(context);
    var biometricAuthentication = false;

    return new Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 40, top: 40, right: 40, bottom: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text('Basics', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                SwitchListTile(
                  title: const Text('Dark Mode'),
                  secondary: Icon(Icons.settings_brightness, color: Theme.of(context).textTheme.body1.color),
                  value: themeChange.darkTheme,
                  onChanged: (bool value) {
                    themeChange.darkTheme = value;
                  }
                ),
                SwitchListTile(
                  title: const Text('Biometric Authentication'),
                  secondary: Icon(Icons.fingerprint, color: Theme.of(context).textTheme.body1.color),
                  value: biometricAuthentication,
                  onChanged: (bool value) {
                    biometricAuthentication = value;
                  }
                )
              ]
            ),
            Padding(
              padding: const EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text('Danger Zone', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                  Text('Be careful down here', style: TextStyle(fontSize: 18)),
                  RaisedButton(
                    onPressed: logOut,
                    child: Text('Log Out'),
                    color: Theme.of(context).accentColor,
                    textColor: Colors.white,
                  ),
                  RaisedButton(
                    onPressed: deleteAccount,
                    child: Text('Delete Account'),
                    color: Theme.of(context).primaryColorDark,
                    textColor: Colors.white,
                  )
                ]
              )
            )
          ]
        )
      )
    );
  }

  logOut() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => WelcomePage()));
  }

  deleteAccount() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String userID = user.uid.toString();

    user.delete().then((val) {

    });

    Firestore.instance.collection('users').document(userID).delete();
    Firestore.instance.collection('usersActivities').document(userID).delete();

    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => WelcomePage()));
  }
}
