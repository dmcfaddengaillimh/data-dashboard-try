import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_dashboard_try/pages/home/home.dart';
import 'package:data_dashboard_try/styles/dark-theme-provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class _InputDropdown extends StatelessWidget {
  static DarkThemeProvider themeChangeProvider = new DarkThemeProvider();

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final VoidCallback onPressed;
  final Widget child;

  const _InputDropdown({
    Key key,
    this.child,
    this.labelText,
    this.valueText,
    this.valueStyle,
    this.onPressed
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: InputDecorator(
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.calendar_today, color: _TimeDataPageState()._newMainColourSwatch),
          labelText: labelText
        ),
        baseStyle: valueStyle,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(valueText, style: valueStyle),
            Icon(Icons.arrow_drop_down,
              color: Theme.of(context).brightness == Brightness.light ? Colors.grey.shade700 : Colors.white70
            )
          ]
        )
      )
    );
  }
}

class _DateTimePicker extends StatelessWidget {
  const _DateTimePicker({
    Key key,
    this.labelText,
    this.selectedDate,
    this.selectedTime,
    this.selectDate,
    this.selectTime,
  }) : super(key: key);

  final String labelText;
  final DateTime selectedDate;
  final TimeOfDay selectedTime;
  final ValueChanged<DateTime> selectDate;
  final ValueChanged<TimeOfDay> selectTime;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000, 1, 1),
      lastDate: DateTime(2100, 12, 31)
    );

    if (picked != null && picked != selectedDate) {
      selectDate(picked);
    }

  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime
    );
    if (picked != null && picked != selectedTime)
      selectTime(picked);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
          child: _InputDropdown(
            labelText: "Date of Activity",
            valueText: DateFormat.yMMMd().format(selectedDate),
            onPressed: () {
              _selectDate(context);
            }
          )
        ),
        const SizedBox(width: 12.0),
        Padding(
          padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
          child: _InputDropdown(
            labelText: "Time of Activity",
            valueText: selectedTime.format(context),
            onPressed: () {
              _selectTime(context);
            }
          )
        )
      ]
    );
  }
}

class TimeDataPage extends StatefulWidget {
  final DocumentSnapshot activitySnapshot;
  final String snapshotID;
  final bool editingEntry;
  const TimeDataPage({Key key, @required this.editingEntry, @optionalTypeArgs this.activitySnapshot, @optionalTypeArgs this.snapshotID}) : super(key: key);

  @override
  _TimeDataPageState createState() => _TimeDataPageState();
}

class _TimeDataPageState extends State<TimeDataPage> {
  // static DarkThemeProvider themeChangeProvider = new DarkThemeProvider();
  DocumentSnapshot activitySnapshot;
  String snapshotID;
  bool editingEntry;

  _TimeDataPageState({this.editingEntry, this.activitySnapshot, this.snapshotID});

  Map _savedIcon;
  String _activityName;
  String _activityDescription;
  String _dateOfActivity;
  String _timeOfActivity;
  String _totalActiveTime;
  String _totalDistractedTime;
  bool _productive = true;
  DateTime _fromDate = DateTime.now();
  TimeOfDay _fromTime = const TimeOfDay(hour: 12, minute: 0);
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final List<String> colourCodeStrings = ['0xffFA8072', '0xffFF8C00', '0xffD2B48C', '0xff1E90FF', '0xff43A047', '0xff4682B4', '0xffBA55D3', '0xff990000', '0xffCC6633'];
  final List<Icon> iconChoices = [
    Icon(Icons.mail),
    Icon(Icons.airplanemode_active),
    Icon(Icons.computer),
    Icon(Icons.people),
    Icon(Icons.call),
    Icon(Icons.fastfood),
    Icon(Icons.school),
    Icon(Icons.personal_video)
  ];

  Color _newMainColourSwatch = Colors.blue;
  String _newMainColour;

  Icon _chosenIcon = Icon(Icons.text_fields, color: Colors.white);

  FocusNode activityNameFocusNode = new FocusNode();
  FocusNode activityDescriptionFocusNode = new FocusNode();
  FocusNode dateFocusNode = new FocusNode();
  FocusNode timeFocusNode = new FocusNode();
  FocusNode productiveFocusNode = new FocusNode();
  FocusNode distractedFocusNode = new FocusNode();

  final TextEditingController _activityNameController = new TextEditingController();
  final TextEditingController _activityDescriptionController = new TextEditingController();
  final TextEditingController _activeTimeController = new TextEditingController();
  final TextEditingController _distractedTimeController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    if(widget.editingEntry) {
      getInitialDetails();
    }
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('Time Entry'),
        backgroundColor: _newMainColourSwatch,
      ),
      body: SingleChildScrollView(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            _dismissKeyboard(context);
          },
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Container(
                  height: 120,
                  color: _newMainColourSwatch,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 40, top: 40, right: 40, bottom: 40),
                    child: TextFormField(
                      validator: (activityNameInput) {
                        if(activityNameInput.isEmpty) {
                          return 'Please enter the name of this activity.';
                        } else {
                          this._activityName = activityNameInput;
                        }
                      },
                      style: TextStyle(color: Colors.white, fontSize: 24),
                      decoration: InputDecoration(
                        hintText: 'Activity Name',
                        hintStyle: TextStyle(color: Colors.white),
                        prefixIcon: _chosenIcon,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide.none
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent, width: 0.0),
                        ),
                      ),
                      controller: _activityNameController,
                      onSaved: (activityNameInput) => _activityName = activityNameInput,
                      onEditingComplete: () {
                        _fieldFocusChange(context, activityNameFocusNode, activityDescriptionFocusNode);
                      },
                      focusNode: activityNameFocusNode,
                      autofocus: true,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  color: Theme.of(context).dialogBackgroundColor,
                  child: ListView.separated(
                    itemCount: 9,
                    scrollDirection: Axis.horizontal,
                    separatorBuilder: (BuildContext context, int index) => Divider(),
                    itemBuilder: (BuildContext context, int index) {
                      return _buildColourCircles(context, index);
                    },
                  )
                ),
                Container(
                  height: 50,
                  color: Theme.of(context).dialogBackgroundColor,
                  child: ListView.separated(
                    itemCount: 8,
                    scrollDirection: Axis.horizontal,
                    separatorBuilder: (BuildContext context, int iconIndex) => Divider(),
                    itemBuilder: (BuildContext context, int iconIndex) {
                      return _buildIconChoices(context, iconIndex);
                    },
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 20, right: 40, bottom: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Productive?'),
                      Checkbox(
                        value: _productive,
                        onChanged: (value) {
                          setState(() {
                            _productive = value;
                          });
                        },
                        checkColor: Colors.white,
                        activeColor: _newMainColourSwatch,
                      )
                    ],
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 20, right: 40, bottom: 30),
                  child: TextFormField(
                    validator: (activityDescriptionInput) {
                      if(activityDescriptionInput.isEmpty) {
                        return 'Please enter a description of this activity.';
                      } else {
                        this._activityDescription = activityDescriptionInput;
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Activity Description',
                      hintText: 'Styling new profile page',
                      prefixIcon: Icon(Icons.info, color: _newMainColourSwatch),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: _newMainColourSwatch,
                        )
                      )
                    ),
                    controller: _activityDescriptionController,
                    onSaved: (activityDescriptionInput) => _activityDescription = activityDescriptionInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, activityDescriptionFocusNode, dateFocusNode);
                    },
                    focusNode: activityDescriptionFocusNode,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(0),
                  child: _DateTimePicker(
                    selectedDate: _fromDate,
                    selectedTime: _fromTime,
                    selectDate: (DateTime date) {
                      if(this.mounted) {
                        setState(() {
                          _fromDate = date;
                          _dateOfActivity = date.toString();
                        });
                      }
                    },
                    selectTime: (TimeOfDay time) {
                      if(this.mounted) {
                        setState(() {
                          _fromTime = time;
                          _timeOfActivity = time.toString();
                        });
                      }
                    },
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 15),
                  child: TextFormField(
                    validator: (totalActiveTimeInput) {
                      if(totalActiveTimeInput.isEmpty) {
                        return 'Please enter the date of this activity.';
                      } else {
                        this._totalActiveTime = totalActiveTimeInput;
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Total Active Time',
                      hintText: 'X minutes',
                      prefixIcon: Icon(Icons.play_circle_filled, color: _newMainColourSwatch),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: _newMainColourSwatch,
                        )
                      )
                    ),
                    controller: _activeTimeController,
                    onSaved: (totalActiveTimeInput) => _totalActiveTime = totalActiveTimeInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, productiveFocusNode, distractedFocusNode);
                    },
                    focusNode: productiveFocusNode,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 15),
                  child: TextFormField(
                    validator: (totalDistractedTimeInput) {
                      if(totalDistractedTimeInput.isEmpty) {
                        return 'Please enter the date of this activity.';
                      } else {
                        this._totalDistractedTime = totalDistractedTimeInput;
                      }
                    },
                    decoration: InputDecoration(
                      labelText: 'Total Distracted Time',
                      hintText: 'Y minutes',
                      prefixIcon: Icon(Icons.pause_circle_filled, color: _newMainColourSwatch),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: _newMainColourSwatch
                        )
                      )
                    ),
                    controller: _distractedTimeController,
                    onSaved: (totalDistractedTimeInput) => _totalDistractedTime = totalDistractedTimeInput,
                    onEditingComplete: () {
                      _fieldFocusChange(context, distractedFocusNode, null);
                    },
                    focusNode: distractedFocusNode,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next
                  ),
                ),
                RaisedButton(
                    onPressed: () => addActivity(),
                    child: Text('Save', style: TextStyle(fontSize: 14, color: Colors.white)),
                    color: Theme.of(context).accentColor,
                  )
              ],
            ),
          ),
        )
      )
    );
  }

  void addActivity() async {
    bool newEntry = widget.editingEntry;

    if(!newEntry) {
      addNewActivity();
    } else {
      updateExistingActivity();
    }
  }

  addNewActivity() async {
    final formState = _formKey.currentState;
    var datestamp = DateTime.parse(_dateOfActivity);
    var activityDate = DateTime(datestamp.year, datestamp.month, datestamp.day, _fromTime.hour, _fromTime.minute);
    bool successfullSubmissionOfChanges;

    if(formState.validate()) {
      formState.save();
      try {
        FirebaseUser user = await FirebaseAuth.instance.currentUser();
        var userID = user.uid.toString();

        Firestore.instance.collection('userActivities').document(userID).collection('loggedActivities').add({
          "activityName": _activityName,
          "activityDescription": _activityDescription,
          "dateOfActivity": activityDate,
          "isProductive": _productive,
          "timeSpent": int.parse(_totalActiveTime),
          "distractedTime": int.parse(_totalDistractedTime),
          "activityIcon": _savedIcon != null ? _savedIcon : {"activityIcon": {"codePoint": 58496, "fontFamily": "MaterialIcons", "matchTextDirection": false}},
          "colour": _newMainColour != null ? _newMainColour : '0xff1E90FF'
        });
        successfullSubmissionOfChanges = true;
        showSnackBar(context, successfullSubmissionOfChanges);
        await Future.delayed(const Duration(seconds: 3));
        Navigator.push(context, new MaterialPageRoute(builder: (context) => Home(user: user)));
      } catch(error) {
        successfullSubmissionOfChanges = false;
        showSnackBar(context, successfullSubmissionOfChanges);
        print(error.message);
      }
    }
  }

  updateExistingActivity() async {
    final formState = _formKey.currentState;
    var datestamp = DateTime.parse(_dateOfActivity);
    var activityDate = DateTime(datestamp.year, datestamp.month, datestamp.day, _fromTime.hour, _fromTime.minute);
    bool successfullSubmissionOfChanges;

    if(formState.validate()) {
      formState.save();
      try {
        FirebaseUser user = await FirebaseAuth.instance.currentUser();
        var userID = user.uid.toString();

        Firestore.instance.collection('userActivities').document(userID).collection('loggedActivities').document(widget.activitySnapshot.documentID).updateData({
          "activityName": _activityName,
          "activityDescription": _activityDescription,
          "dateOfActivity": activityDate,
          "isProductive": _productive,
          "timeSpent": int.parse(_totalActiveTime),
          "distractedTime": int.parse(_totalDistractedTime),
          "activityIcon": _savedIcon != null ? _savedIcon : {"activityIcon": {"codePoint": 58496, "fontFamily": "MaterialIcons", "matchTextDirection": false}},
          "colour": _newMainColour != null ? _newMainColour : '0xff1E90FF'
        });
        successfullSubmissionOfChanges = true;
        showSnackBar(context, successfullSubmissionOfChanges);
        await Future.delayed(const Duration(seconds: 3));
        Navigator.push(context, new MaterialPageRoute(builder: (context) => Home(user: user)));
      } catch(error) {
        successfullSubmissionOfChanges = false;
        showSnackBar(context, successfullSubmissionOfChanges);
        print(error.message);
      }
    }
  }

  showSnackBar(BuildContext context, bool success) {
    Flushbar(
      title: success ? 'Success' : 'Failure',
      message: success ? 'Your activity has been added/updated.' : 'There was an error adding/updating your activity.',
      icon: success ? Icon(
        Icons.check_circle_outline,
        size: 28,
        color: Colors.green,
      ) : Icon(
        Icons.remove_circle_outline,
        size: 28,
        color: Colors.red
      ),
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      shouldIconPulse: false,
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      duration: Duration(seconds: 3),
      flushbarStyle: FlushbarStyle.FLOATING
    ) .. show(context);
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  _dismissKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  _buildColourCircles(BuildContext context, int index) {
    return new Padding(
      padding: const EdgeInsets.only(left: 12, top: 0, right: 10, bottom: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          InkWell(
            child: Container(
              height: 35,
              width: 35,
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                color: Color(int.parse(colourCodeStrings[index])),
              ),
            ),
            onTap: () {
              chooseColour(colourCodeStrings, index);
            }
          )
        ],
      )
    );
  }

  chooseColour(List<String> colourCodes, index) {
    if(this.mounted) {
      setState(() {
        _newMainColourSwatch = Color(int.parse(colourCodes[index]));
        _newMainColour = colourCodes[index];
      });
    }
  }

  _buildIconChoices(BuildContext context, int index) {
    return new Padding(
      padding: const EdgeInsets.only(left: 12, top: 0, right: 10, bottom: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          InkWell(
            child: Container(
              height: 35,
              width: 35,
              child: iconChoices[index]
            ),
            onTap: () {
              chooseIcon(iconChoices[index], index);
            }
          )
        ],
      )
    );
  }

  chooseIcon(Icon icon, index) {
    if(this.mounted) {
      setState(() {
        _chosenIcon = icon;
        _savedIcon = {
          "codePoint": icon.icon.codePoint,
          "fontFamily": icon.icon.fontFamily,
          "matchTextDirection": icon.icon.matchTextDirection
          };
      });
    }
  }

  getInitialDetails() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String userID = user.uid.toString();

    await Firestore.instance.collection('userActivities').document(userID).collection('loggedActivities').document(widget.activitySnapshot.documentID).get()
      .then((document) {
         if(document.exists) {
           if(this.mounted) {
            setState(() {
              _activityNameController.text = document.data['activityName'].toString();
              _activityDescriptionController.text = document.data['activityDescription'].toString();
              _activeTimeController.text = document.data['timeSpent'].toString();
              _distractedTimeController.text = document.data['distractedTime'].toString();
            });
           }
         } else {
           return null;
         }
      });
  }

}
