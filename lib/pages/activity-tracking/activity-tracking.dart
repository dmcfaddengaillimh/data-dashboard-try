import 'package:flutter/material.dart';

import 'package:data_dashboard_try/pages/activity-tracking/stopwatch.dart';

class ActivityTrackingPage extends StatefulWidget {
  @override
  _ActivityTrackingPageState createState() => _ActivityTrackingPageState();
}

class _ActivityTrackingPageState extends State<ActivityTrackingPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Activity Timer'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                NewStopwatch()
              ],
            )
          ]
        )
      )
    );
  }

}
