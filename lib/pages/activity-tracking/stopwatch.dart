import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_dashboard_try/pages/home/home.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class NewStopwatch extends StatefulWidget {
  @override
  _NewStopwatchState createState() => _NewStopwatchState();
}

class _NewStopwatchState extends State<NewStopwatch> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Stopwatch productiveWatch = Stopwatch();
  Stopwatch distractedWatch = Stopwatch();
  Timer productiveTimer;
  Timer distractedTimer;
  bool startStopProductive = true;
  bool startStopDistracted = false;

  String productiveElapsedTime = '00:00:00';
  String distractedElapsedTime = '00:00:00';
  String _activityName;
  String _activityDescription;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(productiveElapsedTime, style: TextStyle(fontSize: 56, color: Theme.of(context).primaryColor)),
                  Text(distractedElapsedTime, style: TextStyle(fontSize: 36, color: Theme.of(context).primaryColorLight)),
                ]
              )
            ],
          ),
          SizedBox(height: 80.0),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        padding: const EdgeInsets.all(30),
                        onPressed: () => startOrStop(),
                        child: Text('Start/Stop Distraction', style: TextStyle(fontSize: 28)),
                        color: Theme.of(context).primaryColorDark,
                        textColor: Colors.white,
                      )
                    )
                  ],
                )
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        padding: const EdgeInsets.all(30),
                        onPressed: () => stopAllClocks(),
                        child: Text('Stop & Log Activity', style: TextStyle(fontSize: 28)),
                        color: Theme.of(context).accentColor,
                        textColor: Colors.white,
                      ),
                    )
                  ],
                )
              )
            ],
          )
        ],
      )
    );
  }

  startOrStop() {
    if(startStopProductive) {
      stopDistractedWatch();
      startWatch();
    } else {
      stopWatch();
      startDistractedWatch();
    }
  }

  startWatch() {
    if(this.mounted) {
      setState(() {
        startStopProductive = false;
        productiveWatch.start();
        productiveTimer = Timer.periodic(Duration(milliseconds: 100), updateTime);
      });
    }
  }

  stopWatch() {
    if(this.mounted) {
      setState(() {
        startStopProductive = true;
        productiveWatch.stop();
        setTime();
      });
    }
  }

  startDistractedWatch() {
    if(this.mounted) {
      setState(() {
        startStopDistracted = true;
        distractedWatch.start();
        distractedTimer = Timer.periodic(Duration(milliseconds: 100), updateTime);
      });
    }
  }

  stopDistractedWatch() {
    if(this.mounted) {
      setState(() {
        startStopDistracted = false;
        distractedWatch.stop();
        setTime();
      });
    }
  }

  stopAllClocks() {
    productiveWatch.stop();
    distractedWatch.stop();
    showModalPopUp();
  }

  showModalPopUp() {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Add Activity'),
          content: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 0, top: 20, right: 0, bottom: 20),
                  child: TextFormField(
                    validator: (activityNameInput) {
                      if(activityNameInput.isEmpty) {
                        return 'Please enter the date of this activity.';
                      } else {
                        this._activityName = activityNameInput;
                      }
                    },
                    autofocus: true,
                    decoration: new InputDecoration(labelText: 'Activity Name', hintText: 'eg. Calling'),
                    onSaved: (activityNameInput) => this._activityName = activityNameInput
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 0, top: 0, right: 0, bottom: 30),
                  child: TextFormField(
                    validator: (activityDescriptionInput) {
                      if(activityDescriptionInput.isEmpty) {
                        return 'Please enter the date of this activity.';
                      } else {
                        this._activityDescription = activityDescriptionInput;
                      }
                    },
                    autofocus: true,
                    maxLines: 4,
                    decoration: new InputDecoration(labelText: 'Activity Description', hintText: 'eg. About the status of the deliverable on Friday'),
                    onSaved: (activityDescriptionInput) => this._activityDescription = activityDescriptionInput
                  )
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text('Cancel', style: TextStyle(color: Theme.of(context).textTheme.body1.color))
                    ),
                    RaisedButton(
                      onPressed: () => saveActivity(),
                      child: Text('Save Activity', style: TextStyle(color: Colors.white)),
                      color: Theme.of(context).primaryColorDark,
                    )
                  ]
                )
              ],
            )
          )
        );
      }
    );
  }

  saveActivity() async {
    final formState = _formKey.currentState;
    bool successfullSubmissionOfChanges;

    if(formState.validate()) {
      formState.save();
      try {
        FirebaseUser user = await FirebaseAuth.instance.currentUser();
        var userID = user.uid.toString();
        Firestore.instance.collection('userActivities').document(userID).collection('loggedActivities').add({
          "activityName": _activityName,
          "activityDescription": _activityDescription,
          "dateOfActivity": Timestamp.now(),
          "isProductive": true,
          "timeSpent": convertToMinutes(productiveWatch),
          "distractedTime": convertToMinutes(distractedWatch),
          "activityIcon": {
            "codePoint": 57672,
            "fontFamily": "MaterialIcons",
            "matchTextDirection": false
          },
          "colour": "0xff1E90FF"
        });
        Navigator.pop(context);
        successfullSubmissionOfChanges = true;
        showSnackBar(context, successfullSubmissionOfChanges);
        await Future.delayed(const Duration(seconds: 3));
        Navigator.push(context, new MaterialPageRoute(builder: (context) => Home(user: user)));
      } catch(error) {
        print(error.message);
      }
    }
  }

  showSnackBar(BuildContext context, bool success) async {
    Flushbar(
      title: success ? 'Success' : 'Failure',
      message: success ? 'Your activity has been saved.' : 'There was an error adding this activity.',
      icon: success ? Icon(
        Icons.check_circle_outline,
        size: 28,
        color: Colors.green,
      ) : Icon(
        Icons.remove_circle_outline,
        size: 28,
        color: Colors.red
      ),
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      shouldIconPulse: false,
      boxShadows: [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      duration: Duration(seconds: 3),
      flushbarStyle: FlushbarStyle.FLOATING
    ) .. show(context);
  }

  convertToMinutes(Stopwatch watch) {
    int milliseconds = watch.elapsedMilliseconds;

    int minutes = (milliseconds / 60000).ceil().toInt();
    if(minutes < 1) {
      minutes = 1;
    }

    return minutes;
  }

  updateTime(Timer timer) {
    if (productiveWatch.isRunning) {
      if(this.mounted) {
        setState(() {
          productiveElapsedTime = transformMilliSeconds(productiveWatch.elapsedMilliseconds);
        });
      }
    } else {
      if(this.mounted) {
        setState(() {
          distractedElapsedTime = transformMilliSeconds(distractedWatch.elapsedMilliseconds);
        });
      }
    }
  }

  setTime() {
    var productiveTimeSoFar = productiveWatch.elapsedMilliseconds;
    var distractedTimeSoFar = distractedWatch.elapsedMilliseconds;

    if(this.mounted) {
      setState(() {
        productiveElapsedTime = transformMilliSeconds(productiveTimeSoFar);
        distractedElapsedTime = transformMilliSeconds(distractedTimeSoFar);
      });
    }
  }

  transformMilliSeconds(int milliseconds) {
    int hundreds = (milliseconds / 10).truncate();
    int seconds = (hundreds / 100).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();

    String hoursStr = (hours % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    return "$hoursStr:$minutesStr:$secondsStr";
  }
}
