import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_dashboard_try/pages/menu-items/profile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AdminHomePage extends StatefulWidget {
  final DocumentSnapshot userSnapshot;
  const AdminHomePage({Key key, @required this.userSnapshot}) : super(key: key);

  @override
  _AdminHomePageState createState() => _AdminHomePageState();
}

class _AdminHomePageState extends State<AdminHomePage> {
  DocumentSnapshot userSnapshot;
  _AdminHomePageState({this.userSnapshot});

  @override
  Widget build(BuildContext context) {
    print(this.userSnapshot);
    return Container(
      child: Text('hello')
    );
  }
}
