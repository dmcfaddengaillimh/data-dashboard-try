import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class TeamMembersPage extends StatelessWidget {
  final String teamName;
  TeamMembersPage({Key key, this.teamName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Team Members')
      ),
      body: FutureBuilder(
        future: Firestore.instance.collection('users').getDocuments(),
        builder: buildMemberList
      ),
    );
  }

  Widget buildMemberList(BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
    if(snapshot.hasData) {
      return ListView.builder(
        itemCount: snapshot.data.documents.length,
        itemBuilder: (context, index) {
          DocumentSnapshot user = snapshot.data.documents[index];

          if(user.data['teamName'] == teamName) {
            return Padding(
              padding: const EdgeInsets.only(left: 40, top: 30, right: 40, bottom: 30),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      InkWell(
                        child: CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 35,
                          backgroundImage: NetworkImage(user.data['profilePhotoURL'])
                        )
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 40),
                              child: Column(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      child: Text(user.data['name']['firstName'], style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
                                    )
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      child: Text(user.data['jobTitle'], textAlign: TextAlign.right, style: TextStyle(fontSize: 14))
                                    )
                                  )
                                ]
                              )
                            )
                          ]
                        )
                      )
                    ]
                  )
                ]
              )
            );
          } else {
            return SizedBox();
          }
        }
      );
    } else if (snapshot.connectionState == ConnectionState.done && !snapshot.hasData) {
        return Center(
            child: Text("No users found."),
        );
    } else {
        return CircularProgressIndicator();
    }
  }
}
