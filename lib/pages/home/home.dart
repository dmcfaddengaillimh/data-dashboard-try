import 'package:data_dashboard_try/pages/administrative/team-dashboard.dart';
import 'package:data_dashboard_try/pages/administrative/team-members.dart';
import 'package:data_dashboard_try/pages/menu-items/dashboard.dart';
import 'package:data_dashboard_try/styles/dark-theme-provider.dart';
import 'package:flutter/material.dart';

import 'package:data_dashboard_try/components/header/header.dart';

import 'package:data_dashboard_try/pages/welcome/welcome.dart';
import 'package:data_dashboard_try/pages/menu-items/settings.dart';
import 'package:data_dashboard_try/pages/time-data/time-data.dart';
import 'package:data_dashboard_try/pages/activity-tracking/activity-tracking.dart';

import 'package:data_dashboard_try/pages/menu-items/profile.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Home extends StatefulWidget {
  const Home({Key key, @optionalTypeArgs this.user}) : super(key: key);
  final FirebaseUser user;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  DarkThemeProvider themeChangeProvider = new DarkThemeProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: StreamBuilder<DocumentSnapshot>(
        stream: Firestore.instance
                .collection('users')
                .document(widget.user.uid)
                .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else if (snapshot.hasData) {
            return checkRole(snapshot.data);
          }

          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return CircularProgressIndicator();
            default:
              return Text(snapshot.data['name']);
          }
        }
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Header(headerText: 'Menu'),
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.person,
                color: Theme.of(context).textTheme.body1.color,
                size: 24.0,
              ),
              title: Text('Profile'),
              subtitle: Text('View and edit your profile details'),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Theme.of(context).textTheme.body1.color,
                size: 24.0,
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.show_chart,
                color: Theme.of(context).textTheme.body1.color,
                size: 24.0,
              ),
              title: Text('Dashboard'),
              subtitle: Text('View your time-tracking data'),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Theme.of(context).textTheme.body1.color,
                size: 24.0,
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage()));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.timelapse,
                color: Theme.of(context).textTheme.body1.color,
                size: 24.0,
              ),
              title: Text('Time Entry'),
              subtitle: Text('Add new activity time logs manually'),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Theme.of(context).textTheme.body1.color,
                size: 24.0,
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => TimeDataPage(editingEntry: false)));
              },
            ),
            ListTile(
              leading: Icon(
                Icons.settings,
                color: Theme.of(context).textTheme.body1.color,
                size: 24.0,
              ),
              title: Text('Settings'),
              subtitle: Text('Edit app settings'),
              trailing: Icon(
                Icons.keyboard_arrow_right,
                color: Theme.of(context).textTheme.body1.color,
                size: 24.0,
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsPage()));
              },
            ),
            ListTile(
              contentPadding: const EdgeInsets.only(left: 20, top: 20, right: 0, bottom: 0),
              leading: Icon(
                Icons.power_settings_new,
                color: Theme.of(context).textTheme.body1.color,
                size: 24.0,
              ),
              title: Text('Log out'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => WelcomePage()));
              },
            ),
            Divider(),
            Container(
              padding: const EdgeInsets.only(left: 0, top: 20, right: 0, bottom: 0),
              child: Text(
                'V 1.0.0',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold)
              ),
            )
          ],
        ),
      ),
    );
  }

  checkRole(DocumentSnapshot documentSnapshot) {
    if (documentSnapshot.data == null) {
      return Center(
        child: Text('No data set in the userId document in firestore.'),
      );
    }
    if (documentSnapshot.data['role'] == 'admin') {
      return adminPage(documentSnapshot);
    } else {
      return userPage(documentSnapshot);
    }
  }

  void getCurrentAppTheme() async {
    themeChangeProvider.darkTheme =
        await themeChangeProvider.darkThemePreference.getTheme();
  }

  adminPage(DocumentSnapshot snapshot) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 40, right: 40, bottom: 0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    InkWell(
                      child: CircleAvatar(
                        backgroundColor: Colors.blue,
                        radius: 50,
                        backgroundImage: NetworkImage('${snapshot.data['profilePhotoURL']}')
                      ),
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));
                      }
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              child: Text('Welcome', textAlign: TextAlign.right, style: TextStyle(fontSize: 36))
                            )
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              child: Text('${snapshot.data['name']['firstName']}', textAlign: TextAlign.right, style: TextStyle(fontSize: 36, color: Theme.of(context).accentColor))
                            )
                          )
                        ]
                      )
                    )
                  ]
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 0, top: 30, right: 0, bottom: 0),
                  child: Container(
                    child: Text('Team leader of: ${snapshot.data['teamName']}', textAlign: TextAlign.left, style: TextStyle(fontSize: 18, color: Theme.of(context).textTheme.body1.color))
                  )
                )
              ]
            )
          ),
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 30, right: 40, bottom: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      RaisedButton(
                        padding: const EdgeInsets.all(40),
                        onPressed: () => {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => TeamMembersPage(teamName: snapshot.data['teamName'])))
                        },
                        child: Text('View Team Members', style: TextStyle(fontSize: 20)),
                        color: Theme.of(context).primaryColor,
                        textColor: Colors.white
                      ),
                    ]
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      RaisedButton(
                        padding: const EdgeInsets.all(40),
                        onPressed: () => {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => TeamDashboard()))
                        },
                        child: Text('View Team Dashboard', style: TextStyle(fontSize: 20)),
                        color: Theme.of(context).accentColor,
                        textColor: Colors.white
                      ),
                    ],
                  )
                )
              ],
            )
          )
        ],
      )
    );
  }

  userPage(DocumentSnapshot snapshot) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 40, top: 40, right: 40, bottom: 0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    InkWell(
                      child: CircleAvatar(
                        backgroundColor: Colors.blue,
                        radius: 50,
                        backgroundImage: NetworkImage('${snapshot.data['profilePhotoURL']}')
                      ),
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));
                      }
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              child: Text('Welcome', textAlign: TextAlign.right, style: TextStyle(fontSize: 36))
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              child: Text('${snapshot.data['name']['firstName']}', textAlign: TextAlign.right, style: TextStyle(fontSize: 36, color: Theme.of(context).accentColor))
                            ),
                          )
                        ],
                      )
                    )
                  ],
                )
              ],
            ),
          ),
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 30, right: 40, bottom: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      RaisedButton(
                        padding: const EdgeInsets.all(40),
                        onPressed: () => {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage()))
                        },
                        child: Text('View Dashboard', style: TextStyle(fontSize: 24)),
                        color: Theme.of(context).primaryColor,
                        textColor: Colors.white,
                      ),
                    ]
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      RaisedButton(
                        padding: const EdgeInsets.all(40),
                        onPressed: () => {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ActivityTrackingPage()))
                        },
                        child: Text('Activity Stopwatch', style: TextStyle(fontSize: 24)),
                        color: Theme.of(context).accentColor,
                        textColor: Colors.white,
                      ),
                    ],
                  )
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40, top: 0, right: 40, bottom: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      RaisedButton(
                        padding: const EdgeInsets.all(40),
                        onPressed: () => {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => TimeDataPage(editingEntry: false)))
                        },
                        child: Text('Activity Input', style: TextStyle(fontSize: 24)),
                        color: Theme.of(context).primaryColorDark,
                        textColor: Colors.white,
                      ),
                    ],
                  )
                )
              ],
            )
          )
        ],
      )
    );
  }
}
