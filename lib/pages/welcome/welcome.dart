import 'package:flutter/material.dart';
import 'dart:ui';

import 'package:data_dashboard_try/components/header/header.dart';
import 'package:data_dashboard_try/components/authentication/login/login.dart';
import 'package:data_dashboard_try/components/authentication/signup/signup.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomPaint(
        painter: PastelPainter(),
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Header(headerText: 'Productivity Timer'),
              ),
              Expanded(
                child: LogoAssetImage()
              ),
              Expanded(
                child: Center(
                  child: Text('Track your time on projects and see where you spend your daily time.', style: TextStyle(color: Colors.white, fontSize: 18.0), textAlign: TextAlign.center),
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        RaisedButton(
                          padding: const EdgeInsets.all(6.0),
                          onPressed: goToLogin,
                          child: Text('Log In', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                          color: Theme.of(context).primaryColor,
                        ),
                        RaisedButton(
                          padding: const EdgeInsets.all(6.0),
                          onPressed: goToSignUp,
                          child: Text('Sign Up', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                          color: Theme.of(context).accentColor,
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          )
        )
      )
    );
  }

  void goToLogin() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => LogInPage(), fullscreenDialog: true));
  }

  void goToSignUp() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpPage(), fullscreenDialog: true));
  }
}

class LogoAssetImage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('images/logo.png');
    Image image = Image(image: assetImage);
    return Container(child: image);
  }
}

class PastelPainter extends CustomPainter {

    @override
    void paint(Canvas canvas, Size size) {
      final height = size.height;
      final width = size.width;

      Paint paint = Paint();

      Path mainBackground = Path();
      mainBackground.addRect(Rect.fromLTRB(0, 0, width, height));
      paint.color = Color(0xFF273741);
      canvas.drawPath(mainBackground, paint);
    }

    @override
    bool shouldRepaint(CustomPainter oldDelegate) {
      return oldDelegate != this;
    }
  }
