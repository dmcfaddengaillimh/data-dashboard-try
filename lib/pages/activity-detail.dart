import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ActivityDetailView extends StatelessWidget {
  DocumentSnapshot activitySnapshot;

  ActivityDetailView({Key key, @required this.activitySnapshot}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(activitySnapshot.data['activityName'].toString()),
        backgroundColor: Color(int.parse(activitySnapshot.data['colour'])),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Stack(
          children: <Widget>[
            new Column(
              children: <Widget>[
                new Container(
                  height: MediaQuery.of(context).size.height * .15,
                  color: Color(int.parse(activitySnapshot.data['colour']))
                ),
                new Container(
                  height: MediaQuery.of(context).size.height * 0.75,
                  color: Theme.of(context).canvasColor,
                ),
              ]
            ),
            Container(
              alignment: Alignment.topCenter,
              padding: new EdgeInsets.only(left: 20, top: MediaQuery.of(context).size.height * .10, right: 20, bottom: 0),
              child: new Container(
                height: 125.0,
                width: MediaQuery.of(context).size.width,
                child: new Card(
                  color: Colors.white,
                  elevation: 5.0,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 40, top: 20, right: 0, bottom: 0),
                        child: Column(
                          children: <Widget>[
                            Text('Active Time', style: TextStyle(color: Colors.black)),
                            Text(activitySnapshot.data['timeSpent'].toString(), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 48, color: Colors.black)),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 40, top: 20, right: 40, bottom: 0),
                        // padding: const EdgeInsets.all(0),
                        child: Column(
                          children: <Widget>[
                            Text('Distracted Time', style: TextStyle(color: Colors.black)),
                            Text(activitySnapshot.data['distractedTime'].toString(), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 48, color: Colors.black))
                          ],
                        )
                      )
                    ]
                  )
                ),
              ),
            ),
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 25, top: 240, right: 40, bottom: 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Activity Description', style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.left),
                      SizedBox(height: 10),
                      Text(activitySnapshot.data['activityDescription'], textAlign: TextAlign.left)
                    ]
                  )
                ),
              ]
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 25, top: 330, right: 0, bottom: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Activity Date', style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.left),
                          SizedBox(height: 10),
                          Text(getDate(), textAlign: TextAlign.left)
                        ]
                      ),
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 0, top: 330, right: 25, bottom: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text('Activity Time', style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.right),
                          SizedBox(height: 10),
                          Text(getTime(), textAlign: TextAlign.right)
                        ]
                      ),
                    )
                  ],
                )
              ],
            ),
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 25, top: 420, right: 0, bottom: 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Activity Type', style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.left),
                      SizedBox(height: 10),
                      Text(checkProductiveActivity(), textAlign: TextAlign.left,)
                    ],
                  )
                )
              ],
            )
          ],
        )
      )
    );
  }

  String getDate() {
    Timestamp activityDate = activitySnapshot.data['dateOfActivity'];
    var newDate = activityDate.toDate();

    var formatter = new DateFormat('EEE, MMMM d, yyyy');
    String formatted = formatter.format(newDate);

    return formatted;
  }

  String getTime() {
    Timestamp activityDate = activitySnapshot.data['dateOfActivity'];
    var newDate = activityDate.toDate();

    var formatter = new DateFormat('h:mm a');
    String formatted = formatter.format(newDate);

    return formatted;
  }

  String checkProductiveActivity() {
    bool productiveActivity = activitySnapshot.data['isProductive'];

    if(productiveActivity) {
      return 'Productive';
    } else {
      return 'Unproductive';
    }

  }

}
